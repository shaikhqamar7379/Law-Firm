import "./App.css";
import Hero from "./components/Hero/Hero";
import Navbar from "./components/Navbar/Navbar";
// import Test from './components/Test'

function App() {
  return (
    <div>
      <Navbar />
      <Hero />
      {/* <Test /> */}
    </div>
  );
}

export default App;
